package com.example.mynoteapptry.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query


@Dao
interface NoteDao {

    @Query("SELECT * FROM NoteEntity")
    fun getAllNote(): List<NoteEntity>

    @Insert()
    fun addNote(noteEntity: NoteEntity)

    @Delete()
    fun deleteNote(noteEntity: NoteEntity)

    @Query("SELECT * FROM NoteEntity where id = :id ORDER BY id DESC LIMIT 1")
    fun getNoteById(id: Long): NoteEntity

    @Query("DELETE FROM NoteEntity")
    fun deleteAllNote()
}