package com.example.mynoteapptry.room

import androidx.lifecycle.MutableLiveData
import com.example.mynoteapptry.repository.Note
import com.example.mynoteapptry.repository.NoteRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
class NoteRepositoryRoom(override val NoteList: MutableLiveData<List<Note>>) : NoteRepository {

    val NoteDao = NoteApplication.database.noteDao()

    override fun getAllNote(): List<Note> {
        val noteList = mutableListOf<Note>()
        CoroutineScope(Dispatchers.IO).launch {
            NoteDao.getAllNote().forEach {
                noteList.add(noteEntitytoNote(it))
            }
        }
        NoteList.value = noteList
        return NoteList.value!!
    }


    override fun addNote(note: Note) {
        CoroutineScope(Dispatchers.IO).launch {
            NoteDao.addNote(noteEntity(note))
        }
    }

    override fun getNote(id: Int): Note? {
        var note: Note? = null
        CoroutineScope(Dispatchers.IO).launch {
            note = noteEntitytoNote(NoteDao.getNoteById(id.toLong()))
        }
        return note
    }

    override fun deleteNote(id: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            NoteDao.deleteNote(NoteDao.getNoteById(id.toLong()))
        }
    }

    override fun deleteAllNote() {
        CoroutineScope(Dispatchers.IO).launch {
            NoteDao.deleteAllNote()
        }
    }

    fun noteEntity(note: Note):NoteEntity {
        return NoteEntity(
            note.
            id.toLong(),
            note.title,
            note.description,
            note.secretNote,
            note.type
        )
    }

    fun noteEntitytoNote(noteEntity: NoteEntity): Note {
        return Note(
            noteEntity.id.toInt(),
            noteEntity.title,
            noteEntity.description,
            noteEntity.category,
            noteEntity.status
        )
    }

}