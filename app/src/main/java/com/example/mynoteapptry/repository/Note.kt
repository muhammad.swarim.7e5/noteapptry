package com.example.mynoteapptry.repository


data class Note(
    val id: Int,
    val title: String,
    val description: String,
    val type: String,
    val secretNote: String
)