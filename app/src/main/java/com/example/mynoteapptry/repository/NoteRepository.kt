package com.example.mynoteapptry.repository

import androidx.lifecycle.MutableLiveData

interface NoteRepository {

    val NoteList: MutableLiveData<List<Note>>

    fun addNote(note: Note)

    fun getNote(id: Int): Note?

    fun deleteNote(id: Int)

    fun getAllNote(): List<Note>

    fun deleteAllNote()
}