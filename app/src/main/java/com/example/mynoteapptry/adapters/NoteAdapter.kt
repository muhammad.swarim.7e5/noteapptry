package com.example.mynoteapptry.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mynoteapptry.OnClickListener
import com.example.mynoteapptry.R
import com.example.mynoteapptry.databinding.ItemBookBinding
import com.example.mynoteapptry.repository.Note

class NoteAdapter(private var noteList: List<Note>, private val listener: OnClickListener):
    RecyclerView.Adapter<NoteAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_book, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val note = noteList[position]
        with(holder) {
            setListener(note)
            binding.titleTextView.text = note.title
            binding.descriptionTextView.text = note.description
        }
    }

    override fun getItemCount(): Int {
        return noteList.size
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val binding = ItemBookBinding.bind(view)
        fun setListener(note: Note) {
            binding.root.setOnClickListener {
                listener.onClick(note)
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setList(newNoteList: List<Note>) {
        noteList = newNoteList
        notifyDataSetChanged()
    }
}