package com.example.mynoteapptry.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mynoteapptry.repository.Note
import com.example.mynoteapptry.ServiceLocator

class NoteViewModel: ViewModel() {

    val noteList = MutableLiveData<MutableList<Note>>(mutableListOf())
    val selectedNote = MutableLiveData<Note>()

    init {
        getAllNote()
    }

    fun getAllNote() {
        ServiceLocator.noteRepository.getAllNote()
        android.os.Handler().postDelayed({
            noteList.postValue(ServiceLocator.noteRepository.NoteList.value!!.toMutableList())
        }, 200)
    }

    fun select(note: Note) {
        selectedNote.postValue(note)
    }

    fun delete(note: Note) {
        ServiceLocator.noteRepository.deleteNote(note.id)
    }

    fun deleteAll() {
        ServiceLocator.noteRepository
    }

    fun addNewNote(note: Note) {
        ServiceLocator.noteRepository.addNote(note)
    }
}