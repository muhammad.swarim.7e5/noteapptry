package com.example.mynoteapptry

import androidx.lifecycle.MutableLiveData
import com.example.mynoteapptry.repository.Note
import com.example.mynoteapptry.repository.NoteRepository
import com.example.mynoteapptry.room.NoteRepositoryRoom


object ServiceLocator {

    val noteRepository: NoteRepository = NoteRepositoryRoom(MutableLiveData<List<Note>>())

}