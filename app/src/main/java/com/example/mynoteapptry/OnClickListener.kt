package com.example.mynoteapptry

import com.example.mynoteapptry.repository.Note

interface OnClickListener {
    fun onClick(note: Note)
}