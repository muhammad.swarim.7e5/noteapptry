package com.example.mynoteapptry.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.example.mynoteapptry.R
import com.example.mynoteapptry.databinding.FragmentDetailBinding
import com.example.mynoteapptry.viewmodels.NoteViewModel


class DetailFragment : Fragment() {

    private val noteViewModel: NoteViewModel by activityViewModels()
    private lateinit var binding: FragmentDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        noteViewModel.selectedNote.observe(viewLifecycleOwner, Observer {
            binding.noteTitleView.text = it.title
            binding.noteView.text = it.description

        })

        binding.deleteButton.setOnClickListener {
            noteViewModel.delete(noteViewModel.selectedNote.value!!)
            Navigation.findNavController(binding.root).navigate(R.id.NoteRecyclerFragment)
        }

    }
}