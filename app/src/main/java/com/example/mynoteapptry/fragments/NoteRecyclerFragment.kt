package com.example.mynoteapptry.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mynoteapptry.OnClickListener
import com.example.mynoteapptry.R
import com.example.mynoteapptry.adapters.NoteAdapter

import com.example.mynoteapptry.databinding.FragmentNoteRecyclerBinding
import com.example.mynoteapptry.repository.Note
import com.example.mynoteapptry.viewmodels.NoteViewModel


class NoteRecyclerFragment : Fragment(), OnClickListener {

    private lateinit var noteAdapter: NoteAdapter
    private lateinit var gridLayoutManager: RecyclerView.LayoutManager
    private val noteViewModel: NoteViewModel by activityViewModels()
    private lateinit var binding: FragmentNoteRecyclerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentNoteRecyclerBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        noteViewModel.getAllNote()

        setUpRecyclerView(noteViewModel.noteList.value!!.toList())
        noteViewModel.noteList.observe(viewLifecycleOwner, Observer {
            noteAdapter.setList(it)

            if (noteViewModel.noteList.value == null) {
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            } else {
                setUpRecyclerView(it)
            }
        })
    }

    private fun setUpRecyclerView(noteList: List<Note>) {
        gridLayoutManager = GridLayoutManager(context, 1)
        if (noteList != null) {
            noteAdapter = NoteAdapter(noteList, this)
            binding.recyclerView.apply {
                setHasFixedSize(true)
                layoutManager = gridLayoutManager
                adapter = noteAdapter
            }
        }
    }


    override fun onClick(note: Note) {
        noteViewModel.select(note)
        findNavController().navigate(R.id.action_NoteRecyclerFragment_to_DetailFragment)
    }
}