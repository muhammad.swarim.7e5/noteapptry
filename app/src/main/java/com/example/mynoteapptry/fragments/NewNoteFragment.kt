package com.example.mynoteapptry.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.mynoteapptry.databinding.FragmentNewBookBinding
import com.example.mynoteapptry.repository.Note
import com.example.mynoteapptry.viewmodels.NoteViewModel

class NewNoteFragment : Fragment() {

    private val noteViewModel: NoteViewModel by activityViewModels()
    private lateinit var binding: FragmentNewBookBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentNewBookBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        val categoryItems = listOf("Action", "Terror", "Romance")
//        val categoryAdapter = ArrayAdapter(requireContext(), R.layout.category_item, categoryItems)
//
//
//        val statusItems = listOf("Read", "To Read", "Reading")
//        val statusAdapter = ArrayAdapter(requireContext(), R.layout.category_item, statusItems)


        binding.newNoteButton.setOnClickListener {
            noteViewModel.addNewNote(
                Note(
                    0,
                    binding.noteTitleField.text.toString(),
                    binding.noteField.text.toString(),
                    binding.noteTitleField.text.toString(),
                    binding.noteField.text.toString()
                )
            )
            binding.noteTitleField.setText("")
            binding.noteField.setText("")

        }

    }
}